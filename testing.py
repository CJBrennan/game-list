import os
import unittest
from app import db, models
addUser = models.Users(username="testname", password="Password")
addGame = models.Games(title="other example")

def logIn(user, password):
    for users in models.Users.query.all():
        if users.username == user\
            and users.password == password:
            return True
    return False

def register(user, password):
    if user and password:
        for users in models.Users.query.all():
            if users.username == user:
                return False
        return True
    else:
        return False

def changePass(user, oldPass, pass1, pass2):
    currentUser = models.Users.query.filter_by(username=user).first()
    if oldPass == currentUser.password and pass1 == pass2:
        return True
    else:
        return False

def addAGame(user, game):
    currentUser = models.Users.query.filter_by(username=user).first()
    exists = models.Games.query.filter_by(title=game).first()
    if exists:
        return False
    else:
        gameToAdd = models.Games(title=game)
        db.session.add(gameToAdd)
        gameToAdd.users.append(currentUser)
        db.session.rollback()
        return True

def removeAGame(user, game):
    currentUser = models.Users.query.filter_by(username=user).first()
    exists = models.Games.query.filter_by(title=game).first()
    try:
        currentUser.game.remove(exists)
        return True
    except:
        return False

class LogIn(unittest.TestCase):
    def setUp(self):
        db.session.add(addUser)
    def tearDown(self):
        db.session.rollback()
    def test_successful_login(self):
        assert logIn("testname", "Password") == True
    def test_wrong_username(self):
        assert logIn("fakename", "Password") == False
    def test_wrong_password(self):
        assert logIn("testname", "Password123") == False

class Register(unittest.TestCase):
    def setUp(self):
        db.session.add(addUser)
    def tearDown(self):
        db.session.rollback()
    def test_successful_register(self):
        assert register("uniquename", "Password") == True
    def test_nonunique_name(self):
        assert register("testname", "Password") == False
    def test_no_password(self):
        assert register("nopassword", "") == False
    def test_no_name(self):
        assert register("", "noname") == False
    def test_no_info(self):
        assert register("", "") == False

class ChangePassword(unittest.TestCase):
    def setUp(self):
        db.session.add(addUser)
    def tearDown(self):
        db.session.rollback()
    def test_successful_change(self):
        assert changePass("testname", "Password", "newpassword", "newpassword") == True
    def test_wrong_old_password(self):
        assert changePass("testname", "wrongpassword", "newpassword", "newpassword") == False
    def test_mismatched_passwords(self):
        assert changePass("testname", "Password", "newpassword", "otherpassword") == False

class AddAGame(unittest.TestCase):
    def setUp(self):
        db.session.add(addUser)
        db.session.add(addGame)
    def tearDown(self):
        db.session.rollback()
    def test_successful_add(self):
        assert addAGame("testname", "example game") == True
    def test_game_already_exists(self):
        assert addAGame("testname", "other example") == False

class RemoveAGame(unittest.TestCase):
    def setUp(self):
        db.session.add(addUser)
        addGame.users.append(addUser)
    def tearDown(self):
        db.session.rollback()
    def test_successful_removal(self):
        assert removeAGame("testname", "other example") == True
    def test_game_does_not_exist(self):
        assert removeAGame("testname", "failed example") == False

if __name__ == '__main__':
    unittest.main()
