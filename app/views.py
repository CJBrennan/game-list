from flask import render_template, flash, redirect, session
from app import app
from app import db, models
from forms import addAGame, signUp, signIn, removeGame, changePass
from datetime import datetime
import hashlib, random

@app.route('/', methods=['GET', 'POST'])
def home():
    if "currentUser" in session:
        return redirect('games')
    return render_template('index.html',\
        title='Game List', username="")

#Calls this function when on the root page of the system
@app.route('/signIn', methods=['GET', 'POST'])
def logIn():
    #Allows the data entered into the form to be accessed
    form = signIn()
    #Once the user has tapped sign in,
        #the entered data is retrieved
    if form.validate_on_submit():
        enteredPass = hashlib.md5()
        user = form.username.data
        #The database of students is searched to see whether
            #the entered details match any of the records
        for users in models.Users.query.all():
            if users.username == user:
                enteredPass.update(form.password.data + users.salt)
                if users.password == enteredPass.hexdigest():
                    #If there is a match, currentUser is updated
                        #and the user is redirected to modules
                    session['currentUser'] = users.username
                    return redirect('games')
        #If a match can't be found, an error is shown
        flash("Username or password incorrect. Please try again.")
    #This displays the contents of singIn.html
    return render_template('signIn.html',\
        title='Sign In', form=form, username="")

#Calls this function when on the root page of the system
@app.route('/register', methods=['GET', 'POST'])
def register():
    #Allows the data entered into the form to be accessed
    form = signUp()
    #Once the user has tapped sign in,
        #the entered data is retrieved
    if form.validate_on_submit():
        pass1 = form.password.data
        pass2 = form.confirmPassword.data
        alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        if pass1 == pass2:
            enteredPass = hashlib.md5()
            passSalt = ''.join(random.choice(alphabet) for i in range(16))
            user = form.username.data
            #The password is stored as a hash, hence why the
            #entered password is hashed before being compared
            enteredPass.update(form.password.data + passSalt)
            #The database of students is searched to see whether
            #the entered details match any of the records
            alreadyExists = models.Users.query.filter_by(username = user).first()
            if alreadyExists == None:
                command = models.Users(username = user,\
                password = enteredPass.hexdigest(), salt = passSalt)
                db.session.add(command)
                db.session.commit()
                flash("Successfully registered!")
            else:
                flash("Username already exists.\nPlease try a different username.")
        else:
            flash("Passwords do not match.\nPlease try again.")
    #This displays the contents of singUp.html
    return render_template('signUp.html',\
        title='Sign Up', form=form, username="")

#Calls this function when on the root page of the system
@app.route('/changePass', methods=['GET', 'POST'])
def changePassword():
    if "currentUser" not in session:
        return redirect('games')

    #Allows the data entered into the form to be accessed
    form = changePass()
    #Once the user has tapped sign in,
        #the entered data is retrieved
    if form.validate_on_submit():
        alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

        user = models.Users.query.filter_by(username=session['currentUser']).first()
        oldPassHash = hashlib.md5()
        oldPassHash.update(form.oldPassword.data + user.salt)
        if oldPassHash.hexdigest() == user.password:
            pass1 = form.newPassword.data
            pass2 = form.confirmPassword.data
            if pass1 == pass2:
                enteredPass = hashlib.md5()
                passSalt = ''.join(random.choice(alphabet) for i in range(16))
                #The password is stored as a hash, hence why the
                #entered password is hashed before being compared
                enteredPass.update(pass1 + passSalt)
                #The database of students is searched to see whether
                #the entered details match any of the records
                user.password = enteredPass.hexdigest()
                user.salt = passSalt
                db.session.commit()
                flash("Password successfully changed!")
            else:
                flash("Passwords do not match.\nPlease try again.")
        else:
            flash("Old passwords don't match\n")
    #This displays the contents of singUp.html
    return render_template('changePass.html',\
        title='Change Password', form=form, username=session['currentUser'])

@app.route('/logOut')
def logOut():
    session.pop('currentUser', None)
    return redirect('/')

@app.route('/games', methods=['GET', 'POST'])
def showGames():
    if "currentUser" not in session:
        return redirect('/')

    games=[]
    user=models.Users.query.filter_by(username=session['currentUser']).first()
    games = user.game

    form = removeGame()
    if form.validate_on_submit():
        removeID = int(form.gameID.data)
        for game in games:
            if game.game_id == removeID:
                user.game.remove(game)
                db.session.commit()
                flash("Game successfully removed!")
                return render_template('games.html', title='Your Library', form=form, gameList=games, username=session['currentUser'])
        flash("A game with that ID was not found. Please try again.")
    return render_template('games.html', title='Your Library', form=form, gameList=games, username=session['currentUser'])

@app.route('/addGame', methods=['GET', 'POST'])
def addGame():
    if "currentUser" not in session:
        return redirect('/')
    global currentUser
    user = models.Users.query.filter_by(username=session['currentUser']).first()
    form = addAGame()
    #Populates the dropdown box seen on the addgame page
    choices = [("Other...", "Other...")]
    for i in models.Games.query.order_by(models.Games.title):
        choices.append((i.title, i.title))
    form.title.choices = choices
    if form.validate_on_submit():
        title = form.title.data
        if title == "Other...":
            title = form.addTitle.data
            if title == "":
                flash("Please enter a game in the text box if selecting other")
                return render_template('addGame.html',\
                    title='Add a Game', form=form, username=session['currentUser'])
            else:
                gameExists = models.Games.query.filter_by(title=title).first()
                if gameExists:
                    gameExists.users.append(user)
                else:
                    #Adds the game to the database
                    addGame = models.Games(title=title)
                    db.session.add(addGame)
                    addGame.users.append(user)
                db.session.commit()
        else:
            addGame = models.Games.query.filter_by(title=title).first()
            addGame.users.append(user)
            db.session.commit()
        try:
            return redirect('/games')
        except:
            flash("An error occurred, please try again.")
    return render_template('addGame.html',\
        title='Add a Game', form=form, username=session['currentUser'])
