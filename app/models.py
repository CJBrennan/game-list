from app import db

userGame = db.Table('userGame',
    db.Column('game_id', db.Integer, db.ForeignKey('games.game_id')),
    db.Column('user_id', db.Integer, db.ForeignKey('users.user_id'))
)

class Users(db.Model):
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), index=True)
    password = db.Column(db.String(32))
    salt = db.Column(db.String(32))
    game = db.relationship('Games', secondary=userGame,\
        backref=db.backref('users', lazy=True))

class Games(db.Model):
    game_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
