from flask_wtf import Form
from wtforms import TextField, PasswordField, SelectField, BooleanField
from wtforms.validators import DataRequired

class signIn(Form):
    username = TextField('username',
        validators=[DataRequired()])
    password = PasswordField('password',
        validators=[DataRequired()])

class signUp(Form):
    username = TextField('username',
        validators=[DataRequired()])
    password = PasswordField('password',
        validators=[DataRequired()])
    confirmPassword = PasswordField('confirmPassword',
        validators=[DataRequired()])

class changePass(Form):
    oldPassword = PasswordField('oldPass',
        validators=[DataRequired()])
    newPassword = PasswordField('newPass',
        validators=[DataRequired()])
    confirmPassword = PasswordField('confirmPass',
        validators=[DataRequired()])

class addAGame(Form):
    title = SelectField('title',
        validators=[DataRequired()], default=-1)
    addTitle = TextField('addTitle')

class removeGame(Form):
    gameID = TextField('gameID', validators=[DataRequired()])
